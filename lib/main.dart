import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/cover_page.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_first_page.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_four_page.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_main_page.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_second_page.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_third_page.dart';
import 'package:final_touchgame/TouchGameGamingView/gameIntroductionPage.dart';
import 'package:final_touchgame/TouchGameGamingView/gamemainpage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(MyApp());
}

// Future main() async{
//   WidgetsFlutterBinding.ensureInitialized();
//   await SaveCustomizeSetting.init();
//   runApp(MyApp());
// }




class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        Provider(create: (_)=> CustomizeSettingBloc()),
      ],
      child: MaterialApp(
        home: CoverPage(),
        routes:{
          '/mainpage': (_)=>CustomizeMainPage(),
          '/firstpage': (_)=>CustomizeFirstPage(),
          '/secondpage': (_)=>CustomizeSecondPage(),
          '/thirdpage': (_)=>CustomizeThirdPage(),
          '/fourpage': (_)=>CustomizeFourPage(),
          '/gameIntropage': (_)=>GameIntroductionPage(),
          '/gamepage': (_)=>GameMainPage(),

        }// home: CustomizeThirdPage(),
      ),
    );
  }
}
