import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_class_setting.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_third_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomizeSecondPage extends StatefulWidget {
  CustomizeSecondPage({Key? key}) : super(key: key);

  @override
  _CustomizeSecondPage createState() => _CustomizeSecondPage();
}

class _CustomizeSecondPage extends State<CustomizeSecondPage> {
  Widget mySlider(CustomizeSettingBloc _bloc) {
    return Container(
      child: StreamBuilder<Setting>(
        stream: _bloc.stateController,
        builder: (context, snapshot) {
          if (snapshot.hasData == false) {
            return SizedBox();
          }
          if (snapshot.data!.lightOffSegmentedControl == 0) {
            return Text("");
          } else {
            return StreamBuilder<Setting>(
                stream: _bloc.stateController,
                builder: (context, snapshot) {
                  if (snapshot.hasData == false) {
                    return SizedBox();
                  }
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1.4,
                        child: Slider(
                          activeColor: Colors.blue[400],
                          inactiveColor: Colors.grey[200],
                          value: snapshot.data!.lightOffTimer.toDouble(),
                          min: 1,
                          max: 20,
                          onChanged: (double value) {
                            _bloc.eventSink.add(LightOffTimer(value.toInt()));
                          },
                        ),
                      ),
                      Text(
                          "${(snapshot.data!.lightOffTimer / 2).toString()} s"),
                    ],
                  );
                });
          }
        },
      ),
    );
  }

  Widget mySlider_two(CustomizeSettingBloc _bloc) {
    return Container(
      child: StreamBuilder<Setting>(
        stream: _bloc.stateController,
        builder: (context, snapshot) {
          if (snapshot.hasData == false) {
            return SizedBox();
          }
          if (snapshot.data!.lightGapSegmentedControl == 0) {
            return Text("");
          } else if (snapshot.data!.lightGapSegmentedControl == 1) {
            return StreamBuilder<Setting>(
                stream: _bloc.stateController,
                builder: (context, snapshot) {
                  if (snapshot.hasData == false) {
                    return SizedBox();
                  }
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1.4,
                        child: Slider(
                          activeColor: Colors.blue[400],
                          inactiveColor: Colors.grey[200],
                          value: snapshot.data!.lightGapCustomTimer.toDouble(),
                          min: 1,
                          max: 20,
                          onChanged: (double value) {
                            _bloc.eventSink.add(LightGapTimer(value.toInt()));
                          },
                        ),
                      ),
                      Text(
                          "${(snapshot.data!.lightGapCustomTimer / 2).toString()} s"),
                    ],
                  );
                });
          } else {
            return StreamBuilder<Setting>(
                stream: _bloc.stateController,
                builder: (context, snapshot) {
                  if (snapshot.hasData == false) {
                    return SizedBox();
                  }
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                          "${(snapshot.data!.lightGapInterFinal[0] / 2).toStringAsFixed(1)} s"),
                      Container(
                        width: MediaQuery.of(context).size.height / 3.5,
                        child: RangeSlider(
                          activeColor: Colors.blue[400],
                          inactiveColor: Colors.grey[200],
                          values: RangeValues(
                              snapshot.data!.lightGapInterFinal[0],
                              snapshot.data!.lightGapInterFinal[1]),
                          min: 1,
                          max: 20,
                          divisions: 19,
                          onChanged: (value) {
                            _bloc.eventSink.add(LightGapInter(value));
                          },
                        ),
                      ),
                      Text(
                          "${(snapshot.data!.lightGapInterFinal[1] / 2).toStringAsFixed(1)} s"),
                    ],
                  );
                });
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _csBloc = Provider.of<CustomizeSettingBloc>(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                                  MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "參數設定",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize:
                              MediaQuery.of(context).devicePixelRatio * 10),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    child: ListView(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.all(10),
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Container(
                                          height: 35,
                                          child: Image.asset(
                                            "assets/images/icons/lightOff.png",
                                          ),
                                        ),
                                      ),
                                      Text(
                                        " 熄燈方式",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: MediaQuery.of(context)
                                                    .devicePixelRatio *
                                                6),
                                        // textScaleFactor: 1.0,
                                      ),
                                    ],
                                  ),
                                ),
                                StreamBuilder<Setting>(
                                    stream: _csBloc.stateController,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData == false) {
                                        return SizedBox();
                                      }
                                      return Container(
                                        width: 400,
                                        child: CupertinoSegmentedControl<int>(
                                            groupValue: snapshot
                                                .data!.lightOffSegmentedControl,
                                            pressedColor: Colors.blue[400],
                                            unselectedColor: Colors.white,
                                            selectedColor: Colors.blue[400],
                                            borderColor: Colors.grey,
                                            children: const <int, Widget>{
                                              0: Text('按壓',
                                                  style:
                                                      TextStyle(fontSize: 15)),
                                              1: Text('按壓&時間',
                                                  style:
                                                      TextStyle(fontSize: 15)),
                                            },
                                            onValueChanged: (int val) {
                                              _csBloc.eventSink
                                                  .add(LightOffSegCtrl(val));
                                            }),
                                      );
                                    }),
                                Expanded(
                                  child: Container(
                                      height: 60, child: mySlider(_csBloc)),
                                ),
                              ],
                            ),
                          ),
                        ), //熄燈方式
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Container(
                                          height: 35,
                                          child: Image.asset(
                                            "assets/images/icons/lightGap.png",
                                          ),
                                        ),
                                      ),
                                      Text(
                                        " 亮燈間隔時間",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: MediaQuery.of(context)
                                                    .devicePixelRatio *
                                                6),
                                        // textScaleFactor: 1.0,
                                      ),
                                    ],
                                  ),
                                ),
                                StreamBuilder<Setting>(
                                    stream: _csBloc.stateController,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData == false) {
                                        return SizedBox();
                                      }
                                      return Container(
                                        width: 400,
                                        child: CupertinoSegmentedControl<int>(
                                            groupValue: snapshot
                                                .data!.lightGapSegmentedControl,
                                            pressedColor: Colors.blue[400],
                                            unselectedColor: Colors.white,
                                            selectedColor: Colors.blue[400],
                                            borderColor: Colors.grey,
                                            children: const <int, Widget>{
                                              0: Text(
                                                '無',
                                                style: TextStyle(
                                                  fontSize: 15,
                                                ),
                                                textScaleFactor: 1.0,
                                              ),
                                              1: Text('自訂時間',
                                                  style:
                                                      TextStyle(fontSize: 15)),
                                              2: Text('區間隨機',
                                                  style:
                                                      TextStyle(fontSize: 15))
                                            },
                                            onValueChanged: (int val) {
                                              _csBloc.eventSink
                                                  .add(LightGapSegCtrl(val));
                                            }),
                                      );
                                    }),
                                Expanded(
                                  child: Container(
                                      height: 60, child: mySlider_two(_csBloc)),
                                ),
                              ],
                            ),
                          ),
                        ), //亮燈間隔時間
                        Padding(
                          padding: EdgeInsets.only(left: 20,right: 20,bottom: 10),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 10,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                              child: Table(
                                columnWidths: <int, TableColumnWidth>{
                                  0: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) / 6),
                                  1: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) / 2.8),
                                  2: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) / 2.2),
                                },
                                defaultVerticalAlignment:
                                TableCellVerticalAlignment.middle,
                                children: [
                                  TableRow(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(8),
                                        child: Image.asset(
                                          "assets/images/icons/rounds.png",
                                        ),
                                      ),
                                      Text("回合間隔時間",
                                          style: TextStyle(
                                              color: Colors.blueGrey,
                                              fontSize: MediaQuery.of(context)
                                                  .devicePixelRatio *
                                                  6)),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                            onPressed: () => _csBloc.eventSink
                                                .add(RemoveRoundGapTimer()),
                                            child: Icon(
                                              Icons.remove,
                                              color: Colors.grey,
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: Colors.grey)),
                                              padding: EdgeInsets.all(5),
                                              primary: Colors.white,
                                            ),
                                          ),
                                          StreamBuilder<Setting>(
                                            stream: _csBloc.stateController,
                                            builder: (context, snapshot) {
                                              if (snapshot.hasData == false) {
                                                return SizedBox();
                                              }
                                              return Container(
                                                width: MediaQuery.of(context).size.width / 15,
                                                child: Center(
                                                  child: Text(
                                                      "${snapshot.data!.roundGapTimer}",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: MediaQuery.of(
                                                              context)
                                                              .devicePixelRatio *
                                                              7)),
                                                ),
                                              );
                                            },
                                          ),
                                          ElevatedButton(
                                            onPressed: () => _csBloc.eventSink
                                                .add(AddRoundGapTimer()),
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.grey,
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: Colors.grey)),
                                              padding: EdgeInsets.all(5),
                                              primary: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ), //回合數
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width / 3,
                              0,
                              MediaQuery.of(context).size.width / 3,
                              0),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/thirdpage');
                            },
                            child: Text("NEXT"),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                          ),
                        ), //NEXT
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
