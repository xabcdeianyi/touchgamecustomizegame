import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_main_page.dart';
import 'package:flutter/material.dart';

class CoverPage extends StatefulWidget {
  CoverPage({Key? key}) : super(key: key);
  @override
  _CoverPageState createState() => _CoverPageState();
}

class _CoverPageState extends State<CoverPage> {

  Widget myView(String title,String subtitle,String picture, Color color) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        height: MediaQuery.of(context).size.height / 6,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 30,
              child: Container(
                alignment: Alignment.topLeft,
                height: MediaQuery.of(context).size.height / 9,
                width: MediaQuery.of(context).size.height / 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title,style: TextStyle(fontSize: MediaQuery.of(context).devicePixelRatio*7,fontWeight: FontWeight.bold),),
                    Text(subtitle,style: TextStyle(height:1.2,fontSize: MediaQuery.of(context).devicePixelRatio*5),),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                height: MediaQuery.of(context).size.height / 6,
                child: Image.asset(
                  "assets/images/mainpage/$picture.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              left: 30,
              bottom: 10,
              child: ElevatedButton(
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => CustomizeMainPage()));
                  Navigator.of(context).pushNamed('/mainpage');
                },
                child: Text("START"),
                style: ElevatedButton.styleFrom(
                  primary: color,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/mainpagebg.jpg",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.fitHeight,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.height / 1.7,
              child: Stack(
                children: [
                  Positioned(
                    top: -20,
                    right: -20,
                    child: Container(
                      width: MediaQuery.of(context).size.height / 2,
                      child: Center(
                        child: Image.asset(
                          "assets/images/mainpage/maintouch.png",
                        ),
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(height: MediaQuery.of(context).size.height / 9),
                      Center(
                        child: Container(
                          width: MediaQuery.of(context).size.height / 3.5,
                          child: Image.asset(
                            "assets/images/mainpage/mainwhiztouch.png",
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 15),
                      Container(
                        height: MediaQuery.of(context).size.height / 1.5,
                        child: ListView(
                          padding: const EdgeInsets.all(50),
                          children: [
                            myView("追逐遊戲","按壓亮燈的裝置","game1", Colors.blue),
                            myView("顏色辨別","聽聲音指示，擊打對應燈光顏色裝置","game2", Colors.green),
                            myView("自訂模式","記住裝置亮燈順序，並依序按壓","gameperson", Colors.purple),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
