import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_first_page.dart';
import 'package:flutter/material.dart';
import 'package:final_touchgame/TouchGameGamingView/gamemainpage.dart';
import 'package:provider/provider.dart';

class CustomizeMainPage extends StatefulWidget {
  CustomizeMainPage({Key? key}) : super(key: key);

  @override
  _CustomizeMainPage createState() => _CustomizeMainPage();
}

class _CustomizeMainPage extends State<CustomizeMainPage> {
  @override
  Widget build(BuildContext context) {
    final _bloc = Provider.of<CustomizeSettingBloc>(context);
    // _bloc.eventSink.add(ReadSetting());
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                              MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )),
                  SizedBox(height: MediaQuery.of(context).size.height / 10),
                  Container(
                    height: MediaQuery.of(context).size.height / 1.6,
                    child: StreamBuilder<Set>(
                        stream: _bloc.SetController,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return SizedBox();
                          }
                          return ListView.builder(
                            padding: const EdgeInsets.all(10),
                            itemCount: snapshot.data!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                child: GestureDetector(
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.height / 10,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.white,
                                    ),
                                    child: Center(
                                        child: Text(
                                            '${snapshot.data!.elementAt(index)}',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: MediaQuery.of(context)
                                                        .devicePixelRatio *
                                                    7))),
                                  ),
                                  onTap: () async {
                                    final setting = await Provider.of<
                                                CustomizeSettingBloc>(context,
                                            listen: false)
                                        .getSetting(
                                            '${snapshot.data!.elementAt(index)}');
                                    Navigator.of(context).pushNamed(
                                        '/gameIntropage',
                                        arguments: {"setting": setting});
                                  },
                                  onLongPress: () {
                                    _bloc.eventSink.add(DeletePage(
                                        snapshot.data!.elementAt(index)));
                                  },
                                ),
                              );
                            },
                          );
                        }), //listBuilder
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/firstpage');
                    },
                    child: Icon(
                      Icons.add,
                      color: Colors.grey,
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(side: BorderSide(color: Colors.grey)),
                      primary: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
