import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_class_setting.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomizeFirstPage extends StatefulWidget {
  CustomizeFirstPage({Key? key}) : super(key: key);

  @override
  _CustomizeFirstPage createState() => _CustomizeFirstPage();
}

class _CustomizeFirstPage extends State<CustomizeFirstPage> {
  String getGroupNumber(int index){
    switch (index){
      case 0:
        return "組別一";
      case 1:
        return "組別二";
      case 2:
        return "組別三";
      case 3:
        return "組別四";
      case 4:
        return "組別五";
      case 5:
        return "組別六";
      default:
        return "";
    }
  }
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<CustomizeSettingBloc>(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                                  MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "參數設定",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize:
                              MediaQuery.of(context).devicePixelRatio * 10),
                    ),
                  ),
                  myContainer(bloc),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width / 3.3,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/secondpage');
                        },
                        child: Text("NEXT"),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                  ), //NEXT
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget myContainer(CustomizeSettingBloc bloc) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.6,
      child: ListView(
        padding: const EdgeInsets.all(10),
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: Table(
                    columnWidths: <int, TableColumnWidth>{
                      0: FixedColumnWidth(
                          (MediaQuery.of(context).size.width - 40) / 8),
                      1: FixedColumnWidth(
                          (MediaQuery.of(context).size.width - 40) / 2.4),
                      2: FixedColumnWidth(
                          (MediaQuery.of(context).size.width - 40) / 2.5),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: [
                      TableRow(
                        children: [
                          Center(
                            child: Image.asset(
                              "assets/images/icons/group.png",
                            ),
                          ),
                          Text(
                            " 組別",
                            style: TextStyle(
                                color: Colors.blueGrey,
                                fontSize:
                                    MediaQuery.of(context).devicePixelRatio *
                                        6),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () =>
                                    bloc.eventSink.add(RemoveGroup()),
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.grey,
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(
                                      side: BorderSide(color: Colors.grey)),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.white,
                                ),
                              ),
                              StreamBuilder<Setting>(
                                stream: bloc.stateController,
                                // stream: _bloc.stateController,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData == false) {
                                    return SizedBox();
                                  }
                                  return Container(
                                    width: 10,
                                    child: Center(
                                      child: Text(
                                          "${snapshot.data!.groupCount}",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: MediaQuery.of(context)
                                                      .devicePixelRatio *
                                                  7)),
                                    ),
                                  );
                                },
                              ),
                              ElevatedButton(
                                onPressed: () => bloc.eventSink.add(AddGroup()),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.grey,
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(
                                      side: BorderSide(color: Colors.grey)),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          Center(
                            child: Image.asset(
                              "assets/images/icons/touch.png",
                            ),
                          ),
                          Text(" Touch 數 (總數)",
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize:
                                      MediaQuery.of(context).devicePixelRatio *
                                          6)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  bloc.eventSink.add(RemoveTouch());
                                },
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.grey,
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(
                                      side: BorderSide(color: Colors.grey)),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.white,
                                ),
                              ),
                              StreamBuilder<Setting>(
                                stream: bloc.stateController,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData == false) {
                                    return SizedBox();
                                  }
                                  return Container(
                                    width: 10,
                                    child: Center(
                                      child: Text(
                                          "${snapshot.data!.touchCount}",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: MediaQuery.of(context)
                                                      .devicePixelRatio *
                                                  7)),
                                    ),
                                  );
                                },
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  bloc.eventSink.add(AddTouch());
                                },
                                child: Icon(
                                  Icons.add,
                                  color: Colors.grey,
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(
                                      side: BorderSide(color: Colors.grey)),
                                  padding: EdgeInsets.all(5),
                                  primary: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          StreamBuilder<Setting>(
              stream: bloc.stateController,
              builder: (context, snapshot) {
                if (snapshot.hasData == false) {
                  print("return sizedbox");
                  return SizedBox();
                }
                return ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data!.groupCount,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 20),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 20,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                        ),
                        child: Center(
                            child: Text(getGroupNumber(index),
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontSize: MediaQuery.of(context)
                                            .devicePixelRatio *
                                        7))),
                      ),
                    );
                  },
                );
              }),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
