import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_class_setting.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:numberpicker/numberpicker.dart';

class CustomizeThirdPage extends StatefulWidget {
  CustomizeThirdPage({Key? key}) : super(key: key);

  @override
  _CustomizeThirdPage createState() => _CustomizeThirdPage();
}

class _CustomizeThirdPage extends State<CustomizeThirdPage> {
  List<int> colorSelect = [
    0xffe57373,
    0xffffb74d,
    0xfffff176,
    0xff81c784,
    0xff64b5f6,
    0xffba68c8
  ];

  Widget mySlider(CustomizeSettingBloc _bloc) {
    return Container(
      child: StreamBuilder<Setting>(
        stream: _bloc.stateController,
        builder: (context, snapshot) {
          if (snapshot.hasData == false) {
            return SizedBox();
          }
          if (snapshot.data!.gameOverSegmentedControl == 0) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                NumberPicker(
                    value: snapshot.data!.gameMinute,
                    minValue: 0,
                    maxValue: 5,
                    onChanged: (value) =>
                        _bloc.eventSink.add(GameMinute(value.toInt()))),
                Text("分",
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: MediaQuery.of(context).devicePixelRatio * 7)),
                NumberPicker(
                    value: snapshot.data!.gameSecond,
                    minValue: 0,
                    step: 5,
                    maxValue: 55,
                    onChanged: (value) =>
                        _bloc.eventSink.add(GameSecond(value.toInt()))),
                Text("秒",
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: MediaQuery.of(context).devicePixelRatio * 7)),
              ],
            );
          } else {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StreamBuilder<Setting>(
                    stream: _bloc.stateController,
                    builder: (context, snapshot) {
                      if (snapshot.hasData == false) {
                        return SizedBox();
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Slider(
                          activeColor: Colors.blue[400],
                          inactiveColor: Colors.grey[200],
                          value: snapshot.data!.gameOverCount.toDouble(),
                          min: 1,
                          max: 99,
                          onChanged: (double value) {
                            _bloc.eventSink.add(GameOverCount(value.toInt()));
                          },
                        ),
                      );
                    }),
                Text("${snapshot.data!.gameOverCount.toString()} ",
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: MediaQuery.of(context).devicePixelRatio * 7)),
              ],
            );
          }
        },
      ),
    );
  }

  Widget myColorPicker(CustomizeSettingBloc _bloc) {
    return StreamBuilder<Setting>(
        stream: _bloc.stateController,
        builder: (context, snapshot) {
          if (snapshot.hasData == false) {
            return SizedBox();
          }
          return ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data!.listTouchColor.length,
            itemBuilder: (BuildContext context, int index) {
              return Table(
                  columnWidths: <int, TableColumnWidth>{
                    0: FixedColumnWidth(
                        (MediaQuery.of(context).size.width - 40) / 6),
                    1: FixedColumnWidth(
                        (MediaQuery.of(context).size.width - 40) / 3),
                    2: FixedColumnWidth(
                        (MediaQuery.of(context).size.width - 40) / 2.2),
                  },
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: [
                    TableRow(
                      children: [
                        SizedBox(height: 0),
                        Text("Touch   ${index + 1}",
                            style: TextStyle(
                                color: Colors.blueGrey,
                                fontSize:
                                    MediaQuery.of(context).devicePixelRatio *
                                        7)),
                        SizedBox(
                          height: 20,
                          width: 30,
                          child: ElevatedButton(
                            onPressed: () async {
                              int result = await showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return myDialog(_bloc, index = index);
                                  });
                              _bloc.eventSink
                                  .add(SetColor(color: result, index: index));
                              // print("set color ${result} and ${index}");
                            },
                            child: Text(""),
                            style: ElevatedButton.styleFrom(
                              primary:
                                  Color(snapshot.data!.listTouchColor[index]),
                              shape: CircleBorder(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ]);
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 30,
              );
            },
          );
        });
  }

  Widget myDialog(CustomizeSettingBloc _bloc, int index) {
    return StreamBuilder<Setting>(
        stream: _bloc.stateController,
        builder: (context, snapshot) {
          if (snapshot.hasData == false) {
            return SizedBox();
          }
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(40.0))),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                for (var i in colorSelect)
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Container(
                      width: MediaQuery.of(context).size.height / 30,
                      child: ElevatedButton(
                        onPressed: () {
                          // snapshot.data!.listTouch[index] = i;
                          Navigator.of(context).pop(i);
                        },
                        child: Text(""),
                        style: ElevatedButton.styleFrom(
                          primary: Color(i),
                          shape: CircleBorder(),
                          // padding: EdgeInsets.all(50),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final _csBloc = Provider.of<CustomizeSettingBloc>(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                                  MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )), //WhizTouch
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "參數設定",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize:
                              MediaQuery.of(context).devicePixelRatio * 10),
                    ),
                  ), //參數設定
                  Container(
                    height: MediaQuery.of(context).size.height / 1.8,
                    child: ListView(
                      padding: EdgeInsets.all(10),
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Table(
                                    columnWidths: <int, TableColumnWidth>{
                                      0: FixedColumnWidth(
                                          (MediaQuery.of(context).size.width -
                                                  40) /
                                              8),
                                      1: FixedColumnWidth(
                                          (MediaQuery.of(context).size.width -
                                                  40) /
                                              2.8),
                                      2: FixedColumnWidth(
                                          (MediaQuery.of(context).size.width -
                                                  40) /
                                              2.2),
                                    },
                                    defaultVerticalAlignment:
                                        TableCellVerticalAlignment.middle,
                                    children: [
                                      TableRow(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.all(2),
                                            child: Image.asset(
                                              "assets/images/icons/step.png",
                                            ),
                                          ),
                                          Text("   顆數",
                                              style: TextStyle(
                                                  color: Colors.blueGrey,
                                                  fontSize: MediaQuery.of(
                                                              context)
                                                          .devicePixelRatio *
                                                      7)),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              ElevatedButton(
                                                onPressed: () {
                                                  _csBloc.eventSink
                                                      .add(RemoveStep());
                                                },
                                                // _bloc.eventSink.add(RemovePlayer()),
                                                child: Icon(
                                                  Icons.remove,
                                                  color: Colors.grey,
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                  shape: CircleBorder(
                                                      side: BorderSide(
                                                          color: Colors.grey)),
                                                  padding: EdgeInsets.all(5),
                                                  primary: Colors.white,
                                                ),
                                              ),
                                              StreamBuilder<Setting>(
                                                stream: _csBloc.stateController,
                                                builder: (BuildContext context,
                                                    AsyncSnapshot<dynamic>
                                                        snapshot) {
                                                  if (snapshot.hasData ==
                                                      false) {
                                                    return SizedBox();
                                                  }
                                                  return Container(
                                                    width: 20,
                                                    child: Center(
                                                      child: Text(
                                                          "${snapshot.data!.listTouchColor.length}",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: MediaQuery.of(
                                                                          context)
                                                                      .devicePixelRatio *
                                                                  7)),
                                                    ),
                                                  );
                                                },
                                              ),
                                              ElevatedButton(
                                                onPressed: () {
                                                  _csBloc.eventSink
                                                      .add(AddStep());
                                                },
                                                // _bloc.eventSink.add(AddPlayer()),
                                                child: Icon(
                                                  Icons.add,
                                                  color: Colors.grey,
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                  shape: CircleBorder(
                                                      side: BorderSide(
                                                          color: Colors.grey)),
                                                  padding: EdgeInsets.all(5),
                                                  primary: Colors.white,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  myColorPicker(_csBloc),
                                ],
                              ),
                            ),
                          ),
                        ), //人數顏色設定
                        Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 3.5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Container(
                                          width: 50,
                                          child: Image.asset(
                                            "assets/images/icons/gameOver.png",
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "  結束方式 ",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: MediaQuery.of(context)
                                                    .devicePixelRatio *
                                                7),
                                      ),
                                    ],
                                  ),
                                ),
                                StreamBuilder<Setting>(
                                    stream: _csBloc.stateController,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData == false) {
                                        return SizedBox();
                                      }
                                      return Container(
                                        width: 400,
                                        child: CupertinoSegmentedControl<int>(
                                            groupValue: snapshot
                                                .data!.gameOverSegmentedControl,
                                            pressedColor: Colors.blue[400],
                                            unselectedColor: Colors.white,
                                            selectedColor: Colors.blue[400],
                                            borderColor: Colors.grey,
                                            children: <int, Widget>{
                                              0: Text('時間',
                                                  style:
                                                      TextStyle(fontSize: 15)),
                                              1: Text('次數',
                                                  style:
                                                      TextStyle(fontSize: 15))
                                            },
                                            onValueChanged: (int val) {
                                              _csBloc.eventSink
                                                  .add(GameOverSegCtrl(val));
                                            }),
                                      );
                                    }),
                                Expanded(
                                  child: Container(child: mySlider(_csBloc)),
                                ),
                              ],
                            ),
                          ),
                        ), //結束方式
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 10,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                              child: Table(
                                columnWidths: <int, TableColumnWidth>{
                                  0: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) /
                                          6),
                                  1: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) /
                                          2.8),
                                  2: FixedColumnWidth(
                                      (MediaQuery.of(context).size.width - 40) /
                                          2.2),
                                },
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                children: [
                                  TableRow(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(8),
                                        child: Image.asset(
                                          "assets/images/icons/rounds.png",
                                        ),
                                      ),
                                      Text("回合數",
                                          style: TextStyle(
                                              color: Colors.blueGrey,
                                              fontSize: MediaQuery.of(context)
                                                      .devicePixelRatio *
                                                  7)),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                            onPressed: () => _csBloc.eventSink
                                                .add(RemoveRound()),
                                            child: Icon(
                                              Icons.remove,
                                              color: Colors.grey,
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: Colors.grey)),
                                              padding: EdgeInsets.all(5),
                                              primary: Colors.white,
                                            ),
                                          ),
                                          StreamBuilder<Setting>(
                                            stream: _csBloc.stateController,
                                            builder: (context, snapshot) {
                                              if (snapshot.hasData == false) {
                                                return SizedBox();
                                              }
                                              return Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    15,
                                                child: Center(
                                                  child: Text(
                                                      "${snapshot.data!.roundCount}",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: MediaQuery.of(
                                                                      context)
                                                                  .devicePixelRatio *
                                                              7)),
                                                ),
                                              );
                                            },
                                          ),
                                          ElevatedButton(
                                            onPressed: () => _csBloc.eventSink
                                                .add(AddRound()),
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.grey,
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: Colors.grey)),
                                              padding: EdgeInsets.all(5),
                                              primary: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ), //回合數
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width / 3.3,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/fourpage');
                        },
                        child: Text("NEXT"),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                  ), //NEXT
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
