import 'dart:convert';

import 'package:final_touchgame/TouchGameCustomizeSetting/customize_bloc.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:final_touchgame/TouchGameCustomizeSettingUI/customize_main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

TextEditingController gamename = new TextEditingController();
TextEditingController gamemethod = new TextEditingController();

class CustomizeFourPage extends StatefulWidget {
  CustomizeFourPage({Key? key}) : super(key: key);

  @override
  _CustomizeFourPage createState() => _CustomizeFourPage();
}

class _CustomizeFourPage extends State<CustomizeFourPage> {
  @override
  Widget build(BuildContext context) {
    final _bloc = Provider.of<CustomizeSettingBloc>(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                                  MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )),
                  SizedBox(height: MediaQuery.of(context).size.height / 15),
                  Text(
                    "遊戲名稱",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize:
                        MediaQuery.of(context).devicePixelRatio * 10),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 20.0, right: 20, top: 30, bottom: 40),
                    child: TextField(
                      controller: gamename,
                      textAlign: TextAlign.left,
                      style: new TextStyle(
                          color: Colors.black,
                          fontSize:
                          MediaQuery.of(context).devicePixelRatio *
                              8),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    ),
                  ),
                  Text(
                    "遊戲說明",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize:
                        MediaQuery.of(context).devicePixelRatio * 10),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 20.0, right: 20, top: 30, bottom: 40),
                    child: TextField(
                      controller: gamemethod,
                      textAlign: TextAlign.left,
                      style: new TextStyle(
                          color: Colors.black,
                          fontSize:
                          MediaQuery.of(context).devicePixelRatio *
                              8),
                      maxLines: 5,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        filled: true, //背景是否填充
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius:
                          BorderRadius.circular(10.0), //新增圓角
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 3.5,
                    child: ElevatedButton(
                      onPressed: () {
                        _bloc.eventSink.add(GameTitle(gamename.text));
                        _bloc.eventSink
                            .add(GameIntroduction(gamemethod.text));
                        _bloc.eventSink.add(SaveSetting());
                        Navigator.of(context).popUntil(
                                (route) => route.settings.name == "/mainpage");
                      },
                      child: Text("SAVE"),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
