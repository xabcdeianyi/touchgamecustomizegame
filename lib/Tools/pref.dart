import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

// setting type = Setting!!!!
Future<Set<String>> sharePreference(String key, String state,
    {dynamic setting = ""}) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  switch (state) {
    case "Write":
      await prefs.setString(key, jsonEncode(setting));
      break;
    case "Delete":
      prefs.remove(key);
      break;
    case "Read":
      break;
    default:
      break;
  }
  return prefs.getKeys();
}
