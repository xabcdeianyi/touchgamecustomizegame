import 'package:final_touchgame/TouchGameGamingBloc/gamelogic.dart';
import 'package:final_touchgame/TouchGameGamingBloc/gamelogic_func.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GameIntroductionPage extends StatefulWidget {
  GameIntroductionPage({Key? key}) : super(key: key);
  @override
  _GameIntroductionPage createState() => _GameIntroductionPage();
}

class _GameIntroductionPage extends State<GameIntroductionPage> {
  final _bloc = GameBloc();

  String getGroupNumber(int index){
    switch (index){
      case 0:
        return "組別一";
      case 1:
        return "組別二";
      case 2:
        return "組別三";
      case 3:
        return "組別四";
      case 4:
        return "組別五";
      case 5:
        return "組別六";
      default:
        return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    dynamic obj = ModalRoute.of(context)!.settings.arguments;
    final setting = obj["setting"]; // 把接收到的參數存到變數
    _bloc.setSetting(setting);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/mainpage/setbg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.contain,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 9),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height:
                              MediaQuery.of(context).devicePixelRatio * 15,
                              child: Image.asset(
                                "assets/images/mainpage/back.png",
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height / 3.5,
                            child: Image.asset(
                              "assets/images/mainpage/mainwhiztouch.png",
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      )),//WhizTouch
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      "遊戲說明",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize:
                          MediaQuery.of(context).devicePixelRatio * 10),
                    ),
                  ), //參數設定
                  Container(
                    height: MediaQuery.of(context).size.height / 1.4,
                    child: ListView(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.all(5),
                      children: [ //結束方式
                        Padding(
                          padding: EdgeInsets.only(left: 20,right: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 6,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: StreamBuilder<GameState>(
                              stream: _bloc.stateStream,
                              builder: (context, snapshot) {
                                if(!snapshot.hasData){return SizedBox();}
                                return Padding(
                                  padding: EdgeInsets.only(left: 20,right: 20,top: 20),
                                  child: SingleChildScrollView(
                                    child: Text("${snapshot.data!.setting.gameIntroduction}",style: TextStyle(
                                        color: Colors.black,
                                        fontSize:
                                        MediaQuery.of(context).devicePixelRatio * 8),),
                                  ),
                                );
                              }
                            ),
                          ),
                        ),
                        StreamBuilder<GameState>(
                            stream: _bloc.stateStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData == false) {
                                return SizedBox();
                              }
                              return Container(
                                height: MediaQuery.of(context).size.height / 2.3 ,
                                child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: snapshot.data!.setting.groupCount,
                                  itemBuilder: (BuildContext context, int index) {
                                    return Padding(
                                      padding: EdgeInsets.only(
                                          left: 20, right: 20, bottom: 10),
                                      child: GestureDetector(
                                        onLongPress:(){
                                          for(int i=1;i <= snapshot.data!.setting.listTouchColor.length;i++)
                                          controlLed(index+1,i,snapshot.data!.setting.listTouchColor[i-1]);
                                        },
                                        child: Container(
                                          height: MediaQuery.of(context).size.height / 20,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                              child: Text(getGroupNumber(index),
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: MediaQuery.of(context)
                                                          .devicePixelRatio *
                                                          7))),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width / 3, right: MediaQuery.of(context).size.width / 3),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/gamepage',arguments: {"setting": setting});
                            },
                            child: Text("NEXT"),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                          ),
                        ), //NEXT
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }



  @override
  void dispose() {
    super.dispose();
  }
}
