import 'package:final_touchgame/TouchGameGamingBloc/gameevent.dart';
import 'package:final_touchgame/TouchGameGamingBloc/gamelogic.dart';
import 'package:final_touchgame/TouchGameGamingBloc/gamelogic_func.dart';
import 'package:flutter/material.dart';

class GameMainPage extends StatefulWidget {
  const GameMainPage({Key? key}) : super(key: key);

  @override
  _GameMainPageState createState() => _GameMainPageState();
}

class _GameMainPageState extends State<GameMainPage> {
  final _bloc = GameBloc();

  int time = 160;
  bool count = false;

  Widget build(BuildContext context) {
    dynamic obj = ModalRoute.of(context)!.settings.arguments;
    final setting = obj["setting"]; // 把接收到的參數存到變數
    _bloc.setSetting(setting);


     Widget buildMainContext(snapshot) {
      if (snapshot.data!.setting.gameOverSegmentedControl ==0) {
        return ListView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data!.setting.groupCount,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Table(
                    columnWidths: <int, TableColumnWidth>{
                      0: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 6),
                      1: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 4),
                      2: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 6),
                      3: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 2.5),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: [
                      TableRow(
                        children: [
                          Image.asset(
                            "assets/images/gamepage/${snapshot.data!.game[index][0]}.png",
                            scale: 1,
                          ),
                          Text("${snapshot.data!.game[index][1]}",
                              style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 20)),
                          Column(children: [
                            Text(
                              "次數",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 22),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 70),
                            Text("${snapshot.data!.game[index][2]}",
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width /
                                            18)),
                          ]),
                          Column(
                            children: [
                              Text(
                                "平均反應時間(秒)",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 22),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 70),
                              Text("${snapshot.data!.game[index][3]}",
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              18)),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 40),
                  Image.asset(
                    "assets/images/gamepage/line.png",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 40),
                ],
              );
            });
      } else if (snapshot.data!.setting.gameOverSegmentedControl ==1) {
        return ListView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data!.setting.groupCount,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Table(
                    columnWidths: <int, TableColumnWidth>{
                      0: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 6),
                      1: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 4),
                      2: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 6),
                      3: FixedColumnWidth(
                          MediaQuery.of(context).size.width / 2.5),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: [
                      TableRow(
                        children: [
                          Image.asset(
                            "assets/images/gamepage/${snapshot.data!.game[index][0]}.png",
                            scale: 1,
                          ),
                          Text("${snapshot.data!.game[index][1]}",
                              style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 20)),
                          Column(children: [
                            Text(
                              "次數",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 22),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 70),
                            Text("${snapshot.data!.game[index][2]}",
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width /
                                            18)),
                          ]),
                          Column(
                            children: [
                              Text(
                                "遊戲時間",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 22),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 70),
                              Text("${snapshot.data!.game[index][3]}",
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              18)),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 40),
                  Image.asset(
                    "assets/images/gamepage/line.png",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 40),
                ],
              );
            });
      }else{
        return SizedBox();
      }
    }



    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Row(
            children: [
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(1));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(2));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(3));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(4));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(5));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(6));
              }),
            ],
          ),
          Image.asset(
            "assets/images/gamepage/gamebg.png",
            fit: BoxFit.fill,
          ),
          FittedBox(
            alignment: Alignment.center,
            fit: BoxFit.fitWidth,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.height / 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 11),
                  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.height / 3.5,
                      child: Image.asset(
                        "assets/images/mainpage/mainwhiztouch.png",
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 20),
                  StreamBuilder<GameState>(
                      stream: _bloc.stateStream,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return SizedBox();
                        }
                        return Container(
                          width: MediaQuery.of(context).size.height / 5,
                          height: MediaQuery.of(context).size.height / 5,
                          decoration: new BoxDecoration(
                            border: Border.all(
                              width: 6,
                              color: Color(0xffA6BDD8),
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                              child: Text(
                            "${snapshot.data!.showtime}",
                            textScaleFactor:
                                MediaQuery.of(context).size.width / 150,
                          )),
                        );
                      }),
                  SizedBox(height: MediaQuery.of(context).size.height / 13),
                  StreamBuilder<GameState>(
                    stream: _bloc.stateStream,
                    builder: (context, snapshot) {
                      if(!snapshot.hasData){return Container();}
                      return Container(
                        height: MediaQuery.of(context).size.height / 2,
                        decoration: BoxDecoration(
                            // color: Colors.red,
                            ),
                        child: buildMainContext(snapshot),
                      );
                    }
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            left: MediaQuery.of(context).size.width / 8,
            top: MediaQuery.of(context).size.height / 2.8,
            child: GestureDetector(
              onTap: () {
                count = false;
                _bloc.eventSink.add(Restart());
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 8,
                child: Image.asset(
                  "assets/images/gamepage/restart.png",
                ),
              ),
            ),
          ),
          StreamBuilder<GameState>(
              stream: _bloc.stateStream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return SizedBox();
                }
                return Positioned(
                  right: MediaQuery.of(context).size.width / 8,
                  top: MediaQuery.of(context).size.height / 2.8,
                  child: GestureDetector(
                    onTap: () {
                      _bloc.eventSink.add(Start());
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 8,
                      child: Image.asset(
                        "${statusToIconAsset(snapshot.data!.status)}",
                      ),
                    ),
                  ),
                );
              }),
          Positioned(
            top: MediaQuery.of(context).size.height / 9,
            left: MediaQuery.of(context).size.height / 40,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: MediaQuery.of(context).devicePixelRatio * 15,
                child: Image.asset(
                  "assets/images/mainpage/back.png",
                ),
              ),
            ),
          ),
          Row(
            children: [
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(1));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(2));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(3));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(4));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(5));
              }),
              FloatingActionButton(onPressed: () {
                _bloc.eventSink.add(TouchPress(6));
              }),
            ],
          ),
        ],
      ),
    );
  }
}
