import 'package:final_touchgame/TouchGameCustomizeSetting/customize_class_setting.dart';
import 'package:final_touchgame/TouchGameGamingBloc/gamelogic_func.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:final_touchgame/TouchGameGamingBloc/gameevent.dart';


enum GameStatus { Ready, Readyforstart, Started, Paused }

List<BuildGame> buildGameList = [];


class GameState {
  GameStatus status = GameStatus.Ready;
  late int time;
  late String showtime;
  late List<List> game;
  late Setting setting;
  late List<List> touchPressListen;


}

class BuildGame {
  late int times = 0; //次數
  late int reactTime = 0; //反應時間 or 完成時間
  late int round = 1; //回合數
  late int step = 1; // 步數

  void lightOffTimer() {
    const oneSec = const Duration(seconds: 1);
    new Timer.periodic(
      oneSec,
          (Timer timer) {

      },
    );
  }

// Future<List> lightOff()async {
//   //lightOffSegmentedControl: 0是按壓;1是按壓或時間
//   // if(state.setting.lightOffSegmentedControl == 0){
//   //   return
//   // }else{
//   //   return
//   // }
// }

}

GameState state = GameState();
BuildGame gameState = BuildGame();


class GameBloc {
  //選染用的list
  List<List> buildGame = [
    ["red", "組別一", "0", "0"],
    ["orange", "組別二", "0", "0"],
    ["yellow", "組別三", "0", "0"],
    ["green", "組別四", "0", "0"],
    ["blue", "組別五", "0", "0"],
    ["purple", "組別六", "0", "0"],
  ];


  //觸發事件
  BehaviorSubject<GameEvent> _eventController = BehaviorSubject();

  StreamSink<GameEvent> get eventSink => _eventController.sink;

  //game state
  BehaviorSubject<GameState> _stateController = BehaviorSubject();

  StreamSink<GameState> get stateSink => _stateController.sink;

  Stream<GameState> get stateStream => _stateController.stream;


  GameBloc() {
    _eventController.stream.listen(_mapEventToState);
  }

  setSetting(Setting setting) {
    state.setting = setting;
    stateSink.add(state);
    state.game =
        List.generate(setting.groupCount, (index) => buildGame[index]);
    state.time = 4;
    state.showtime = "00:00";
    for (int i = 1; i <= _stateController.value!.setting.groupCount; i++) {
      BuildGame i = BuildGame();
      buildGameList.add(i);
    }
    state.touchPressListen = List.generate(
        _stateController.value!.setting.groupCount, (index) =>
        List.generate(_stateController.value!.setting.listTouchColor.length, (
            index) => false));
  }

  void _mapEventToState(GameEvent event) {
    if (event is Restart) {
      _stateController.value!.status = GameStatus.Paused;
      // startTimer();
      _stateController.value!.time = 4;
      _stateController.value!.status = GameStatus.Readyforstart;
    } else if (event is Start)  {
      if (_stateController.value!.status == GameStatus.Ready) {
        _stateController.value!.status = GameStatus.Readyforstart;
        startTimer();
      } else {
        if (_stateController.value!.status == GameStatus.Paused) {
          _stateController.value!.status = GameStatus.Started;
          startTimer();
        } else if (_stateController.value!.status == GameStatus.Started) {
          _stateController.value!.status = GameStatus.Paused;
        }
      }
    } else if (event is TimerCount) {
      if (_stateController.value!.status == GameStatus.Readyforstart) {
        _stateController.value!.time--;
        _stateController.value!.showtime =
            _stateController.value!.time.toString();
        if (_stateController.value!.time == 0) {
          _stateController.value!.status = GameStatus.Started;
          _stateController.value!.time = setGameTime();
          _stateController.value!.showtime =
              timeToString(_stateController.value!.time);
        }
      } else {
        // startGame();
        if (state.setting.gameOverSegmentedControl == 0) {
          _stateController.value!.time--;
          _stateController.value!.showtime =
              timeToString(_stateController.value!.time);
        }
        else if (state.setting.gameOverSegmentedControl == 1) {
          _stateController.value!.time++;
          _stateController.value!.showtime =
              timeToString(_stateController.value!.time);
        }
      }
    } else if (event is TouchPress) {
      pressTouch(event.num);
      state.touchPressListen[numToStep(event.num)[0] - 1][numToStep(
          event.num)[1] - 1] = true;
    }
    stateSink.add(_stateController.value!);
  }

  void dispose() {
    _eventController.close();
  }

  int setGameTime() {
    if (_stateController.value!.setting.gameOverSegmentedControl == 0) {
      return _stateController.value!.setting.gameDuration;
    } else {
      return 1;
    }
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_stateController.value!.status == GameStatus.Readyforstart) {
          eventSink.add(TimerCount());
        } else if (_stateController.value!.time <= 0 ||
            _stateController.value!.status == GameStatus.Paused) {

        } else {
          eventSink.add(TimerCount());
        }
      },
    );
  }
}









