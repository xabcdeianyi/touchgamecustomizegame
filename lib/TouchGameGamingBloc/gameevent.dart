import 'package:flutter/material.dart';

abstract class GameEvent {}

class Init extends GameEvent{}

class Start extends GameEvent{}

class Restart extends GameEvent{}

class Pause extends GameEvent{}

class TimerCount extends GameEvent{}

class TouchPress extends GameEvent{
  TouchPress(this.num);
  int num;
}