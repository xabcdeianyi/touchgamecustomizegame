import 'package:final_touchgame/TouchGameGamingBloc/gamelogic.dart';

String timeToString(int time) {
  String? min;
  String? sec;
  if (time ~/ 60 < 10) {
    min = "0${(time ~/ 60).toString()}";
  } else {
    min = "${(time ~/ 60).toString()}";
  }
  if (time % 60 < 10) {
    sec = "0${(time % 60).toString()}";
  } else {
    sec = "${(time % 60).toString()}";
  }
  String timeString = "${min}:${sec}";
  return timeString;
}

String statusToIconAsset(GameStatus status) {
  switch (status) {
    case GameStatus.Ready:
      return "assets/images/gamepage/start.png";
    case GameStatus.Readyforstart:
      return "assets/images/gamepage/pause.png";
    case GameStatus.Started:
      return "assets/images/gamepage/pause.png";
    case GameStatus.Paused:
      return "assets/images/gamepage/start.png";
  }
}

void controlLed(int group, int step, int color) {
  print(group);
  print(step);
  print(color);
}

List numToStep(int num) {
  int group = (num / state.setting.listTouchColor.length).ceil();
  int step = num % state.setting.listTouchColor.length;
  return [group, step];
}

// Future<void> startGame() async {
//   for (int i = 1; i <= state.setting.groupCount; i++) {
//     controlLed(i, 1, state.setting.listTouchColor[0]);
//   } //第一步燈亮
//
// //   for (int i = 1; i <= state.setting.groupCount; i++) {
// //   await
// // } //熄燈function
// }

void lightOn(int group, int step) {
  controlLed(group, step, state.setting.listTouchColor[step-1]);
}

void pressTouch(int num) {
  if (numToStep(num)[1] == buildGameList[numToStep(num)[0]-1].step){
    stepPlus(numToStep(num)[0]);  //換題目
    lightOn(numToStep(num)[0],  buildGameList[numToStep(num)[0]-1].step);//return stepCount
  }
}

void stepPlus(int group){
  if (buildGameList[group-1].step  == state.setting.listTouchColor.length){
    buildGameList[group-1].step = 0;
    buildGameList[group-1].times += 1;  //加分
  }
  buildGameList[group-1].step += 1;
}
