// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customize_class_setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Setting _$SettingFromJson(Map<String, dynamic> json) {
  return Setting(
    (json['listTouchColor'] as List<dynamic>).map((e) => e as int).toList(),
    (json['lightGapInterFinal'] as List<dynamic>)
        .map((e) => (e as num).toDouble())
        .toList(),
    roundGapTimer: json['roundGapTimer'] as int,
    groupCount: json['groupCount'] as int,
    touchCount: json['touchCount'] as int,
    lightGapSegmentedControl: json['lightGapSegmentedControl'] as int,
    lightOffTimer: json['lightOffTimer'] as int,
    lightOffSegmentedControl: json['lightOffSegmentedControl'] as int,
    lightGapCustomTimer: json['lightGapCustomTimer'] as int,
    gameOverSegmentedControl: json['gameOverSegmentedControl'] as int,
    gameDuration: json['gameDuration'] as int,
    gameMinute: json['gameMinute'] as int,
    gameSecond: json['gameSecond'] as int,
    gameOverCount: json['gameOverCount'] as int,
    roundCount: json['roundCount'] as int,
    gameTitle: json['gameTitle'] as String,
    gameIntroduction: json['gameIntroduction'] as String,
  );
}

Map<String, dynamic> _$SettingToJson(Setting instance) => <String, dynamic>{
      'roundGapTimer': instance.roundGapTimer,
      'groupCount': instance.groupCount,
      'touchCount': instance.touchCount,
      'lightOffSegmentedControl': instance.lightOffSegmentedControl,
      'lightOffTimer': instance.lightOffTimer,
      'lightGapSegmentedControl': instance.lightGapSegmentedControl,
      'lightGapCustomTimer': instance.lightGapCustomTimer,
      'lightGapInterFinal': instance.lightGapInterFinal,
      'listTouchColor': instance.listTouchColor,
      'gameOverSegmentedControl': instance.gameOverSegmentedControl,
      'gameDuration': instance.gameDuration,
      'gameMinute': instance.gameMinute,
      'gameSecond': instance.gameSecond,
      'gameOverCount': instance.gameOverCount,
      'roundCount': instance.roundCount,
      'gameTitle': instance.gameTitle,
      'gameIntroduction': instance.gameIntroduction,
    };
