import 'package:flutter/material.dart';

abstract class CustomizeSettingEvent {}

class AddGroup extends CustomizeSettingEvent {}

class RemoveGroup extends CustomizeSettingEvent {}

class AddRound extends CustomizeSettingEvent {}

class RemoveRound extends CustomizeSettingEvent {}

class AddTouch extends CustomizeSettingEvent {}

class RemoveTouch extends CustomizeSettingEvent {}

class SetColor extends CustomizeSettingEvent {
  SetColor({required this.color, required this.index});

  int color = 0xffe57373;
  int index = 0;
}

class LightOffSegCtrl extends CustomizeSettingEvent {
  LightOffSegCtrl(this.value);

  int value = 0;
}

class LightGapSegCtrl extends CustomizeSettingEvent {
  LightGapSegCtrl(this.value);

  int value = 0;
}

class LightOffTimer extends CustomizeSettingEvent {
  LightOffTimer(this.value);

  int value;
}

class LightGapTimer extends CustomizeSettingEvent {
  LightGapTimer(this.value);

  int value;
}

class LightGapInter extends CustomizeSettingEvent {
  LightGapInter(this.value);

  RangeValues value;
}

class GameOverSegCtrl extends CustomizeSettingEvent {
  GameOverSegCtrl(this.value);

  int value = 0;
}

class GameOverCount extends CustomizeSettingEvent {
  GameOverCount(this.value);

  int value;
}

class GameTitle extends CustomizeSettingEvent {
  GameTitle(this.string);

  String string;
}

class GameIntroduction extends CustomizeSettingEvent {
  GameIntroduction(this.string);
  String string;
}

class GameMinute extends CustomizeSettingEvent {
  GameMinute(this.value);
  int value;
}

class GameSecond extends CustomizeSettingEvent {
  GameSecond(this.value);
  int value;
}

class AddStep extends CustomizeSettingEvent {}

class RemoveStep extends CustomizeSettingEvent {}

class SaveSetting extends CustomizeSettingEvent {}

class ReadSetting extends CustomizeSettingEvent {}

class DeletePage extends CustomizeSettingEvent {
  DeletePage(this.value);
  String value;
}

class AddRoundGapTimer extends CustomizeSettingEvent{}

class RemoveRoundGapTimer extends CustomizeSettingEvent{}
