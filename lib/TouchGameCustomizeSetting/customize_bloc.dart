import 'dart:async';
import 'package:final_touchgame/Tools/pref.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_event.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:final_touchgame/TouchGameCustomizeSetting/customize_class_setting.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';


class CustomizeSettingBloc {
  //Stream
  BehaviorSubject<Setting> StateController =
      new BehaviorSubject.seeded(Setting([0xffe57373], [5, 17]));

  StreamSink<Setting> get _inStateController => StateController.sink;

  Stream<Setting> get stateController => StateController.stream;

  BehaviorSubject<Set> SetController = new BehaviorSubject();

  StreamSink<Set> get _inSetController => SetController.sink;

  Stream<Set> get setController => SetController.stream;

  //SharedPreference
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<String> gameData;

  late Set<String> gameName;

  Future<void> sharePrefrence(key, String state) async {
    final gameName =
        await sharePreference(key, state, setting: StateController.value);
    _inSetController.add(gameName);
  }

  Future<Setting> getSetting(key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = prefs.getString(key) ?? "";
    final setting = Setting.fromJson(json.decode(data));
    return setting;
  }

  //觸發事件
  BehaviorSubject<CustomizeSettingEvent> _eventController =
      new BehaviorSubject();

  StreamSink<CustomizeSettingEvent> get eventSink => _eventController.sink;

  CustomizeSettingBloc() {
    _eventController.stream.listen(_mapEventToState);
    sharePrefrence("", "Read");
  }

  void _mapEventToState(CustomizeSettingEvent event) {
    var setting = StateController.value ?? Setting([0xffe57373], [5, 17]);
    void addGroup () {
      setting.listTouchColor = [0xffe57373];
      if (setting.groupCount <= 5) {
        if (setting.touchCount <= setting.groupCount) {
          setting.touchCount++;
        }
        setting.groupCount++;
      }
    }
    void removeGroup(){
      setting.listTouchColor = [0xffe57373];
      if (setting.groupCount > 1) {
        setting.groupCount--;
      }
    }
    void addTouch (){
      setting.listTouchColor = [0xffe57373];
      if (setting.touchCount < 6) {
        setting.touchCount++;
      }
    }
    void removeTouch (){
      setting.listTouchColor = [0xffe57373];
      if (setting.touchCount > 1) {
        if (setting.touchCount <= setting.groupCount) {
          setting.groupCount--;
        }
        setting.touchCount--;
      }
    }
    void addRoundGapTimer(){
      if (setting.roundGapTimer < 20) {
        setting.roundGapTimer++;
      }
    }
    void removeRoundGapTimer(){
      if (setting.roundGapTimer > 1) {
        setting.roundGapTimer--;
      }
    }

    switch (event.runtimeType) {
      case AddGroup:
        addGroup();
        break;
      case RemoveGroup:
        removeGroup();
        break;
      case AddTouch:
        addTouch();
        break;
      case RemoveTouch:
        removeTouch();
        break;
      case AddRoundGapTimer:
        addRoundGapTimer();
        break;
      case RemoveRoundGapTimer:
        removeRoundGapTimer();
        break;
      default:
        break;
    }




    if (event is AddRound) {
      if (setting.roundCount < 10) setting.roundCount++;
    } else if (event is RemoveRound) {
      if (setting.roundCount > 1) setting.roundCount--;
    } else if (event is LightOffSegCtrl) {
      setting.lightOffSegmentedControl = event.value;
    } else if (event is LightOffTimer) {
      setting.lightOffTimer = event.value;
    } else if (event is LightGapSegCtrl) {
      setting.lightGapSegmentedControl = event.value;
    } else if (event is LightGapTimer) {
      setting.lightGapCustomTimer = event.value;
    } else if (event is LightGapInter) {
      setting.lightGapInterFinal[0] = event.value.start;
      setting.lightGapInterFinal[1] = event.value.end;
    } else if (event is GameTitle) {
      setting.gameTitle = event.string;
    } else if (event is GameIntroduction) {
      setting.gameIntroduction = event.string;
    } else if (event is GameOverCount) {
      setting.gameOverCount = event.value;
    } else if (event is GameOverSegCtrl) {
      setting.gameOverSegmentedControl = event.value;
    } else if (event is GameMinute) {
      setting.gameMinute = event.value;
      setting.gameDuration = (event.value) * 60 + (setting.gameSecond);
    } else if (event is GameSecond) {
      setting.gameSecond = event.value;
      setting.gameDuration = (setting.gameMinute) * 60 + (event.value);
    } else if (event is AddStep) {
      if (setting.listTouchColor.length <
          (setting.touchCount ~/ setting.groupCount))
        setting.listTouchColor.add(0xffe57373);
    } else if (event is RemoveStep) {
      if (setting.listTouchColor.length > 1)
        setting.listTouchColor.removeLast();
    } else if (event is SetColor) {
      setting.listTouchColor[event.index] = event.color;
    } else if (event is SaveSetting) {
      sharePrefrence(setting.gameTitle, "Write");
    } else if (event is ReadSetting) {
      sharePrefrence("", "Read");
    } else if (event is DeletePage) {
      sharePrefrence(event.value, "Delete");
    }
    _inSetController.add(SetController.value!);
    _inStateController.add(setting);
  }


  void dispose() {
    _eventController.close();
    StateController.close();
  }
}
