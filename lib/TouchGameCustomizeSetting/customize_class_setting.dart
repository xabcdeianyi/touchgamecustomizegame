import 'package:json_annotation/json_annotation.dart';
part 'customize_class_setting.g.dart';

@JsonSerializable()
class Setting {
  Setting(this.listTouchColor, this.lightGapInterFinal,
      {this.groupCount = 1,
        this.roundGapTimer = 1,
        this.touchCount = 1,
        this.lightGapSegmentedControl = 0,
        this.lightOffTimer = 1,
        this.lightOffSegmentedControl = 0,
        this.lightGapCustomTimer = 1,
        this.gameOverSegmentedControl = 0,
        this.gameDuration = 0,
        this.gameMinute = 0,
        this.gameSecond = 0,
        this.gameOverCount = 1,
        this.roundCount = 1,
        this.gameTitle = "",
        this.gameIntroduction = ""});

  //First Page
  int groupCount = 1; //group 數量
  int touchCount = 1; //touch 數量
  //Second Page
  int lightOffSegmentedControl = 0; //熄燈方式切換
  int lightOffTimer = 1; //熄燈方式秒數
  int lightGapSegmentedControl = 0; //亮燈間隔切換
  int lightGapCustomTimer = 1; //亮燈間隔時間
  List<double> lightGapInterFinal = [5, 17]; //亮燈間隔區間
  int roundGapTimer = 1;

  //Third Page
  List<int> listTouchColor = [0xffe57373];
  int gameOverSegmentedControl = 0; //結束方式切換
  int gameDuration = 0; //結束方式時間
  int gameMinute = 0;
  int gameSecond = 0;
  int gameOverCount = 1; //結束方式次數
  int roundCount = 1; //回合數
  //Fourth Page
  String gameTitle = ""; //遊戲名稱
  String gameIntroduction = "";

  //遊戲說明

  factory Setting.fromJson(Map<String, dynamic> json) =>
      _$SettingFromJson(json);

  Map<String, dynamic> toJson() => _$SettingToJson(this);
}